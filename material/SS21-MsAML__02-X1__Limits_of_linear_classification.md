# Exercise 02-1: Limits of linear classification [[PDF](SS21-MsAML__02-X1_Limits_of_linear_classification.pdf)]

Two sets $X, Y\subset \mathbb R^m$ are said to be linearly separable if there
are $w\in\mathbb R^m$ and $b\in\mathbb R$ such that either
* $\forall x\in X, y\in Y: w\cdot x+b \geq 0 \quad \wedge \quad w\cdot y + b < 0$

or 
* $\forall x\in X, y\in Y: w\cdot x+b < 0 \quad \wedge \quad w\cdot y + b \geq 0$.

Recall that the range of the Perceptron algorithm contains only hypotheses of the
form $h(x)=\text{sign}(w\cdot x+b)$ for to be specified $w,b$. Accordingly,
the Perceptron can only learn to "perfectly" classify data sets that are also
linearly separable. Let us ask if this suffices to learn elementary logic.

For this task, let us take a look at the primitive logical gates. For example,
the AND gate is define by:

| Input 1 | Input 2 | Output |
| -       | -       | -      |
| 0       | 0       | 0      |
| 0       | 1       | 0      |
| 1       | 0       | 0      |
| 1       | 1       | 1      |

1. Would a Perceptron be able to learn this AND gate? To answer this question,
   translate the input/output table above into our supervised learning setting:
   * What is the feature space, class space, training data, and learning goal?
   * Is the training data linearly separable according to the above definition?

2. Can all these primitive logical gates, i.e., all combinations of $2^{2x2}$
   input/output combinations, be learned by the Perceptron? If not, give an
   example and state which percentage of classifications would be incorrect.

