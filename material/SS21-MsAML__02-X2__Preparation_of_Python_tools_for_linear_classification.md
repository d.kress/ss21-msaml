# Exercise 02-2: Preparation of Python tools for linear classification [[PDF](SS21-MsAML__02-X2_Preparation_of_Python_tools_for_linear_classification.pdf)]

As mentioned on page 8 of [Linear classification and Perceptron
definition](SS21-MsAML__02-1__Linear_classification.pdf)
module, we need several tools to visualize and analyze the performance of
linear classifiers. Let us prepare these tools in this exercises:

1. Load the Iris data set and separate them into two classes:
   * Class -1: Iris setosa
   * Class +1: All other types
2. Shuffle the data and divide it into two data sets of
   * 60% training data and
   * 40% test data
   
   For this, there are several ready-to-use routines available. Take a look
   at, e.g., at one of those:
   * https://docs.scipy.org/doc/numpy-1.14.0/reference/generated/numpy.random.shuffle.html#numpy.random.shuffle 
   * https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.sample.html
   * https://scikit-learn.org/stable/modules/generated/sklearn.utils.shuffle.html
3. Produce a scatter plot with the training data represented by, e.g., little
   crosses having a class dependent color, e.g., red for class -1 and blue for
   class +1.
4. Define a hypothesis
   $h(x):=\text{sign}(w\cdot x+b)$
   by means of specifying the weights (or normal vector) $w$ and bias (or
   displacement) $b$.
5. Choose some values for $w$ and $b$ manually and shade the regions in the
   scatter plot according the predicted class labels -1 and +1.
6. Automatically count the number of correct/incorrect classification of the
   training data and potentially perhaps optimize your first choice of $w$
   and $b$ by hand.
7. Finally, also plot the test data into the same figure using the same colors
   for the two classes but, e.g., with a dot instead of a cross per data points
   and count the correct/incorrect classifications of the test data. Depending
   on your random sample of the training data, observe any potential bias, and
   potentially adapt the splitting to 80% training data and 20% test data to
   reduce this bias.
8. Is a perfect classification by means of such linear classifiers $h$ above
   even possible?


